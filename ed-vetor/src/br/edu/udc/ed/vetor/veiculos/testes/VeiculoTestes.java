package br.edu.udc.ed.vetor.veiculos.testes;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.ed.vetor.Vetor;
import br.edu.udc.ed.vetor.veiculos.Veiculo;

public class VeiculoTestes {

	// Testes com estrura montada em sala

	@Test
	public void adicionaDezMilVeiculos() {
		final Vetor<Veiculo> veiculos = new Vetor<>();
		
		final long agora = System.currentTimeMillis();

		for (int i = 0; i < 10000000; i++) {
			Veiculo veiculo = new Veiculo();
			veiculos.adiciona(veiculo);
		}

		final long depois = System.currentTimeMillis();

		double tempo = (depois - agora) / 1000.0;
		System.out.println("Adicionar 10 mil sem API - Tempo em segundos = " + tempo);

		Assert.assertEquals(10000000, veiculos.tamanho());
	}

	@Test
	public void adicionaVeiculoNaPosicaoMil() {
		final Vetor<Veiculo> veiculos = new Vetor<>();
		
		final long agora = System.currentTimeMillis();

		for (int i = 0; i < 10000000; i++) {
			Veiculo veiculo = new Veiculo();
			veiculos.adiciona(veiculo);
		}

		Veiculo veiculo = new Veiculo();
		veiculos.adiciona(1000, veiculo);

		final long depois = System.currentTimeMillis();

		double tempo = (depois - agora) / 1000.0;
		System.out.println("Adicionar na posi��o mil sem API - Tempo em segundos = " + tempo);

		Assert.assertEquals(10000001, veiculos.tamanho());
	}

	// Testes com API

	@Test
	public void adicionaDezMilVeiculosComAPI() {
		final List<Veiculo> list = new ArrayList<>();
		
		final long agora = System.currentTimeMillis();

		for (int i = 0; i < 10000000; i++) {
			Veiculo veiculo = new Veiculo();
			list.add(veiculo);
		}

		final long depois = System.currentTimeMillis();

		double tempo = (depois - agora) / 1000.0;
		System.out.println("Adicionar 10 mil com API - Tempo em segundos = " + tempo);

		Assert.assertEquals(10000000, list.size());
	}

	@Test
	public void adicionaVeiculoNaPosicaoMilComAPI() {
		final List<Veiculo> list = new ArrayList<>();

		final long agora = System.currentTimeMillis();

		for (int i = 0; i < 10000000; i++) {
			Veiculo veiculo = new Veiculo();
			list.add(veiculo);
		}

		Veiculo veiculo = new Veiculo();
		list.add(1000, veiculo);

		final long depois = System.currentTimeMillis();

		double tempo = (depois - agora) / 1000.0;
		System.out.println("Adicionar na posi��o mil com API - Tempo em segundos = " + tempo);

		Assert.assertEquals(10000001, list.size());
	}

}
