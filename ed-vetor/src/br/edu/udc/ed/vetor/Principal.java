package br.edu.udc.ed.vetor;

public class Principal {

	public static void main(String[] args) {
		Vetor<Integer> vetor = new Vetor<>();
		
		for(int i = 0; i < 10; i++) {
			vetor.adiciona(i);
		}
		
		System.out.println(vetor);
		
		vetor.embaralha();
		
		System.out.println(vetor);
	}
	
}
