package br.edu.udc.ed.vetor.testes;

import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.ed.vetor.VetorObjects;
import br.edu.udc.ed.vetor.alunos.Aluno;

public class VetorObjectsTestes {
	
	@Test
	public void adicionaDevePassar() {
		final VetorObjects vetor = new VetorObjects();
		vetor.adiciona(new Aluno());
		vetor.adiciona("Uma String");
		vetor.adiciona(1);
		vetor.adiciona(1L);
		vetor.adiciona(System.out);
		
		Assert.assertEquals(4, vetor.tamanho());
		System.out.println(vetor);
	}
	
	@Test
	public void adicionaDevePassarGenerico() {
		final VetorObjects vetor = new VetorObjects();
		vetor.adiciona("String");
		vetor.adiciona(1L);
		vetor.adiciona('c');
		
		 //final String c = (String) vetor.obtem(2);
		
	}
}
