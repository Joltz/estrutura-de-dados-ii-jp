package br.edu.udc.ed.tabela.produto.testes;

import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.ed.tabela.produto.Produto;
import br.edu.udc.ed.tabela.produto.TabelaProduto;
import br.edu.udc.ed.vetor.Vetor;

public class ProdutoTeste {

	@Test
	public void adicionaDevePassar() {
		final TabelaProduto<Produto> produtos = new TabelaProduto<>();

		final Produto produto = new Produto();
		produto.setNome("Chocolate");
		produtos.adiciona(produto);

		final Produto produtoDois = new Produto();
		produtoDois.setNome("Salgadinho");
		produtos.adiciona(produtoDois);

		final Produto produtoTres = new Produto();
		produtoTres.setNome("Morango");
		produtos.adiciona(produtoTres);

		Assert.assertEquals(3, produtos.tamanho());
		Assert.assertTrue(produtos.contem(produto));
		Assert.assertTrue(produtos.contem(produtoDois));
		Assert.assertTrue(produtos.contem(produtoTres));
	}

	@Test
	public void adicionaDoisIguaisDevePassar() {
		final TabelaProduto<Produto> produtos = new TabelaProduto<>();

		produtos.adiciona(new Produto());
		produtos.adiciona(new Produto());
		produtos.adiciona(new Produto());

		Assert.assertEquals(1, produtos.tamanho());
	}
	
	
	@Test(expected=NullPointerException.class)
	public void adicionaDeveFalhar() {
		final TabelaProduto<Produto> produtos = new TabelaProduto<>();

		final Produto produto = null;
		
		produtos.adiciona(produto);
		
		Assert.fail("Adiciona falhou, resultado esperado Null Pointer Excpetion.");
	}
	
	@Test
	public void removeDevePassar() {
		final TabelaProduto<Produto> produtos = new TabelaProduto<>();

		final Produto produtoUm = new Produto();
		produtoUm.setNome("Chocolate");
		produtos.adiciona(produtoUm);

		final Produto produtoDois = new Produto();
		produtoDois.setNome("Salgadinho");
		produtos.adiciona(produtoDois);

		produtos.remove(produtoUm);

		Assert.assertEquals(1, produtos.tamanho());
		Assert.assertFalse(produtos.contem(produtoUm));
	}

	@Test(expected=RuntimeException.class)
	public void removeDeveFalhar() {
		final TabelaProduto<Produto> produtos = new TabelaProduto<>();

		final Produto produto = new Produto();

		produtos.remove(produto);

		Assert.fail("Deveria ter dado um erro de nao remover.");
	}

	@Test(expected = RuntimeException.class)
	public void removeDeveFalharRemovendo2Vezes() {
		final TabelaProduto<Produto> produtos = new TabelaProduto<>();

		final Produto produto = new Produto();
		produtos.adiciona(produto);
		produtos.remove(produto);
		produtos.remove(produto);

		Assert.fail("Deveria ter dado um erro de n�o remover o segundo.");
	}
	
	@Test
	public void listarTodasDevePassar() {
		final TabelaProduto<Produto> produtos = new TabelaProduto<>();
		
		final Produto produtoUm = new Produto();
		produtoUm.setNome("Chocolate");
		produtos.adiciona(produtoUm);
		
		final Produto produtoDois = new Produto();
		produtoDois.setNome("Salgadinho");
		produtos.adiciona(produtoDois);
		
		final Produto produtoTres = new Produto();
		produtoTres.setNome("Pipoca");
		produtos.adiciona(produtoTres);
		
		Assert.assertEquals(3, produtos.tamanho());
		
		final Vetor<Produto> vetorProdutos = produtos.todas();
		Assert.assertEquals(3, vetorProdutos.tamanho());
	}
	
	@Test
	public void todasDevePassarComVazio() {
		final TabelaProduto<Produto> produtos = new TabelaProduto<>();
		
		Assert.assertEquals(0, produtos.todas().tamanho());
	}
	
	@Test
	public void contemDevePassar() {
		final TabelaProduto<Produto> produtos = new TabelaProduto<>();
		
		final Produto produto = new Produto();
		
		produtos.adiciona(produto);
		
		Assert.assertTrue(produtos.contem(produto));
		Assert.assertEquals(1, produtos.tamanho());
	}
	
	@Test
	public void contemDeveRetornarFalso() {
		final TabelaProduto<Produto> produtos = new TabelaProduto<>();
		
		final Produto produto = new Produto();
		
		Assert.assertFalse(produtos.contem(produto));
		Assert.assertEquals(0, produtos.tamanho());
	}
	
	@Test(expected=NullPointerException.class)
	public void contemDeveFalhar() {
		final TabelaProduto<Produto> produtos = new TabelaProduto<>();
		
		final Produto produto = null;
		
		produtos.contem(produto);
		
		Assert.fail("Espera o retorno de um Null Pointer Exception.");
	}
	

}
