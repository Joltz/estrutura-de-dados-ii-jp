package br.edu.aula.ed;

public interface IColecao<T> {
	int tamanho();
	boolean contem(T object);
	Iterador<T> iterador();
}
