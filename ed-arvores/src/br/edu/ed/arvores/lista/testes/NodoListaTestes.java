package br.edu.ed.arvores.lista.testes;

import org.junit.Test;
import org.junit.Assert;

import br.edu.ed.arvores.NodoAbstrato;
import br.edu.ed.arvores.lista.NodoLista;
import br.edu.udc.ed.vetor.Vetor;

public class NodoListaTestes {
	
	@Test
	public void adicionaDevePassar() {
		// Fluent Interface
		final NodoLista<String> organograma = new NodoLista<>("F�bio");
		final NodoLista<String> angela = organograma.adicionar("Angela");
		final NodoLista<String> alessandra = angela.adicionar("Alessandra");
		alessandra.adicionar("Rodrigo");
		alessandra.adicionar("Jo�o");
		alessandra.adicionar("miguel");

		Assert.assertEquals(3, alessandra.grau());
		//Assert.assertEquals(6, organograma.tamanhoArvore());
		Assert.assertEquals(0, organograma.altura());
	}

	@Test
	public void adicionaNaPosicaoDevePassar() {
		final NodoLista<String> organograma = new NodoLista<>("Fabio");
		organograma.adicionar("Angela 1", 0);
		organograma.adicionar("Angela 2", 1);
		organograma.adicionar("Angela 3", 2);
		organograma.adicionar("Angela 4");

		Assert.assertEquals("Angela 1", organograma.getFilhos().obtem(0).getElemento());
		Assert.assertEquals("Angela 2", organograma.getFilhos().obtem(1).getElemento());
		Assert.assertEquals("Angela 3", organograma.getFilhos().obtem(2).getElemento());
		Assert.assertEquals("Angela 4", organograma.getFilhos().obtem(3).getElemento());

	}

	@Test
	public void grauComFilhosDevePassar() {
		final NodoLista<String> arvore = new NodoLista<>("R");

		arvore.adicionar("Filho 1");
		arvore.adicionar("Filho 2");
		arvore.adicionar("Filho 3");

		Assert.assertEquals(3, arvore.grau());
	}

	@Test
	public void irmaoDevePassar() {
		final NodoAbstrato<String> texto = new NodoLista<>("Alfabeto");
		final NodoAbstrato<String> a = texto.adicionar("A");
		final NodoAbstrato<String> a1 = a.adicionar("A1");
		final NodoAbstrato<String> a2 = a.adicionar("A3");
		final NodoAbstrato<String> a3 = a.adicionar("A2");
		final NodoAbstrato<String> b = texto.adicionar("b");
		final NodoAbstrato<String> b1 = a.adicionar("B1");

		Assert.assertTrue(a.irmaoDe(b));
		Assert.assertTrue(a1.irmaoDe(a2));
		Assert.assertTrue(a3.irmaoDe(a3));

		Assert.assertFalse(a.irmaoDe(a1));
		Assert.assertFalse(b1.irmaoDe(a));
	}

	@Test
	public void descendenteAscendenteDevePassar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoLista<String> a = alfabeto.adicionar("a");
		final NodoLista<String> a1 = a.adicionar("a1");
		final NodoLista<String> a11 = a1.adicionar("a11");

		final NodoLista<String> b = alfabeto.adicionar("b");
		final NodoLista<String> c = alfabeto.adicionar("c");

		Assert.assertTrue(alfabeto.ancestralDe(a11));
		Assert.assertTrue(a.ancestralDe(a1));
		Assert.assertTrue(a.ancestralDe(a11));

		Assert.assertFalse(a11.ancestralDe(a));
		Assert.assertFalse(b.ancestralDe(a1));

		Assert.assertTrue(a1.descendenteDe(a));
		Assert.assertTrue(b.descendenteDe(alfabeto));

		Assert.assertFalse(c.ancestralDe(alfabeto));

	}

	@Test
	public void caminhoSistemaDeArquivosDevePassar() {
		final NodoLista<String> c = new NodoLista<String>("C:/");
		final NodoLista<String> windows = c.adicionar("Windows/");
		final NodoLista<String> system32 = windows.adicionar("System 32/");
		system32.adicionar("temp/").getPai().adicionar("drivers/").getPai().adicionar("xovo/").getPai()
				.adicionar("xnxx/");

		final NodoLista<String> fonts = windows.adicionar("fonts");
		final Vetor<NodoAbstrato<String>> caminho = c.caminho(fonts);

		Assert.assertEquals(3, c.comprimento(fonts));
		Assert.assertEquals(3, caminho.tamanho());
	}

	@Test
	public void grauDevePassar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoLista<String> a = alfabeto.adicionar("a");
		final NodoLista<String> a1 = a.adicionar("a1");
		a1.adicionar("a11");
		a1.adicionar("a12");

		final NodoLista<String> b = alfabeto.adicionar("b");
		b.adicionar("b1");
		b.adicionar("b2");
		b.adicionar("b3");

		Assert.assertEquals(1, a.grau());
		Assert.assertEquals(2, a1.grau());
		Assert.assertEquals(3, b.grau());
	}

	@Test
	public void grauSemFilhosDevePassar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoLista<String> a = alfabeto.adicionar("a");
		final NodoLista<String> a1 = a.adicionar("a1");

		Assert.assertEquals(0, a1.grau());
	}

	@Test
	public void externoDevePassar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoLista<String> a = alfabeto.adicionar("a");
		final NodoLista<String> a1 = a.adicionar("a1");

		Assert.assertEquals(0, a1.grau());
		Assert.assertTrue(a1.externo());
	}

	@Test
	public void externoDeveRetornarFalso() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoLista<String> a = alfabeto.adicionar("a");
		final NodoLista<String> a1 = a.adicionar("a1");
		a1.adicionar("a11");
		a1.adicionar("a12");

		Assert.assertEquals(2, a1.grau());
		Assert.assertFalse(a1.externo());
	}

	@Test
	public void internoDevePassar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoLista<String> a = alfabeto.adicionar("a");
		final NodoLista<String> a1 = a.adicionar("a1");
		a1.adicionar("a11");
		a1.adicionar("a12");

		Assert.assertEquals(2, a1.grau());
		Assert.assertTrue(a1.interno());
	}

	@Test
	public void internoDeveRetornarFalso() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoLista<String> a = alfabeto.adicionar("a");
		final NodoLista<String> a1 = a.adicionar("a1");

		Assert.assertEquals(0, a1.grau());
		Assert.assertFalse(a1.interno());
	}

	@Test
	public void irmaoDeDevePassar() {
		final NodoAbstrato<String> texto = new NodoLista<>("Alfabeto");
		final NodoAbstrato<String> a = texto.adicionar("A");
		final NodoAbstrato<String> a1 = a.adicionar("A1");
		final NodoAbstrato<String> a2 = a.adicionar("A3");
		final NodoAbstrato<String> a3 = a.adicionar("A2");
		final NodoAbstrato<String> b = texto.adicionar("b");

		Assert.assertTrue(a.irmaoDe(b));
		Assert.assertTrue(a1.irmaoDe(a2));
		Assert.assertTrue(a3.irmaoDe(a3));
	}

	@Test
	public void irmaoDeDeveRetornarFalso() {
		final NodoAbstrato<String> texto = new NodoLista<>("Alfabeto");
		final NodoAbstrato<String> a = texto.adicionar("A");
		final NodoAbstrato<String> a1 = a.adicionar("A1");
		final NodoAbstrato<String> b1 = a.adicionar("B1");

		Assert.assertFalse(a.irmaoDe(a1));
		Assert.assertFalse(b1.irmaoDe(a));
	}

	@Test
	public void raizDevePassar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoAbstrato<String> a = alfabeto.adicionar("a");
		alfabeto.adicionar("b");
		alfabeto.adicionar("c");

		Assert.assertTrue(alfabeto.raiz());
		Assert.assertEquals(alfabeto, a.getRaiz());
	}

	@Test
	public void raizDeveRetornarFalso() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoAbstrato<String> a = alfabeto.adicionar("a");
		final NodoAbstrato<String> b = alfabeto.adicionar("b");
		alfabeto.adicionar("c");

		Assert.assertFalse(a.raiz());
		Assert.assertNotEquals(b, a.getRaiz());
	}

	@Test
	public void formaArestaComDevePassar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoAbstrato<String> a = alfabeto.adicionar("a");
		final NodoAbstrato<String> a1 = a.adicionar("a1");
		final NodoAbstrato<String> a2 = a.adicionar("a2");
		final NodoAbstrato<String> b = alfabeto.adicionar("b");
		alfabeto.adicionar("c");

		Assert.assertTrue(a.formaArestaCom(alfabeto));
		Assert.assertTrue(a1.formaArestaCom(a));
		Assert.assertTrue(a2.formaArestaCom(a));
		Assert.assertTrue(b.formaArestaCom(alfabeto));
	}

	@Test
	public void formaArestaComDeveRetornarFalso() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoAbstrato<String> a = alfabeto.adicionar("a");
		final NodoAbstrato<String> a1 = a.adicionar("a1");
		final NodoAbstrato<String> a2 = a.adicionar("a2");
		final NodoAbstrato<String> b = alfabeto.adicionar("b");
		alfabeto.adicionar("c");

		Assert.assertFalse(a.formaArestaCom(b));
		Assert.assertFalse(a1.formaArestaCom(b));
		Assert.assertFalse(a2.formaArestaCom(b));
		Assert.assertFalse(b.formaArestaCom(a));
	}

	@Test
	public void getPaidDevePassar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoAbstrato<String> a = alfabeto.adicionar("a");
		final NodoAbstrato<String> a1 = a.adicionar("a1");
		final NodoAbstrato<String> a2 = a.adicionar("a2");
		final NodoAbstrato<String> b = alfabeto.adicionar("b");
		alfabeto.adicionar("c");

		Assert.assertEquals(a.getPai(), alfabeto);
		Assert.assertEquals(a1.getPai(), a);
		Assert.assertEquals(a2.getPai(), a);
		Assert.assertEquals(b.getPai(), alfabeto);
	}

	@Test
	public void getPaiDeveSerFalso() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoAbstrato<String> a = alfabeto.adicionar("a");
		final NodoAbstrato<String> a1 = a.adicionar("a1");
		final NodoAbstrato<String> a2 = a.adicionar("a2");
		final NodoAbstrato<String> b = alfabeto.adicionar("b");
		alfabeto.adicionar("c");

		Assert.assertNotEquals(a.getPai(), b);
		Assert.assertNotEquals(a1.getPai(), alfabeto);
		Assert.assertNotEquals(a2.getPai(), b);
		Assert.assertNotEquals(b.getPai(), a);
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void getPaiDeveFalhar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		alfabeto.getPai();

		Assert.fail("Deveria ter dado um erro de n�o conter pai.");
	}

	@Test
	public void getRaizDevePassar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoAbstrato<String> a = alfabeto.adicionar("a");
		final NodoAbstrato<String> a1 = a.adicionar("a1");
		final NodoAbstrato<String> a2 = a.adicionar("a2");
		final NodoAbstrato<String> b = alfabeto.adicionar("b");
		alfabeto.adicionar("c");

		Assert.assertEquals(a.getRaiz(), alfabeto);
		Assert.assertEquals(a1.getRaiz(), alfabeto);
		Assert.assertEquals(a2.getRaiz(), alfabeto);
		Assert.assertEquals(b.getRaiz(), alfabeto);
	}

	@Test
	public void getRaizDevePassarRetornarFalso() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoAbstrato<String> a = alfabeto.adicionar("a");
		final NodoAbstrato<String> a1 = a.adicionar("a1");
		final NodoAbstrato<String> a2 = a.adicionar("a2");
		final NodoAbstrato<String> b = alfabeto.adicionar("b");
		alfabeto.adicionar("c");

		Assert.assertEquals(a.getRaiz(), alfabeto);
		Assert.assertEquals(a1.getRaiz(), alfabeto);
		Assert.assertEquals(a2.getRaiz(), alfabeto);
		Assert.assertEquals(b.getRaiz(), alfabeto);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getRaizDeveFalhar() {
		final NodoLista<String> alfabeto = new NodoLista<String>(null);
		alfabeto.getRaiz();

		Assert.fail("Deveria ter dado um erro de raiz nula.");
	}

	@Test
	public void getElementoDevePassar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoAbstrato<String> a = alfabeto.adicionar("a");
		final NodoAbstrato<String> a1 = a.adicionar("a1");
		final NodoAbstrato<String> a2 = a.adicionar("a2");
		final NodoAbstrato<String> b = alfabeto.adicionar("b");
		alfabeto.adicionar("c");

		Assert.assertEquals(a.getElemento(), "a");
		Assert.assertEquals(a1.getElemento(), "a1");
		Assert.assertEquals(a2.getElemento(), "a2");
		Assert.assertEquals(b.getElemento(), "b");
	}

	@Test
	public void getElementoDeveFalhar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoAbstrato<String> a = alfabeto.adicionar("a");
		final NodoAbstrato<String> a1 = a.adicionar("a1");
		final NodoAbstrato<String> a2 = a.adicionar("a2");
		final NodoAbstrato<String> b = alfabeto.adicionar("b");
		alfabeto.adicionar("c");

		Assert.assertNotEquals(a.getElemento(), "b");
		Assert.assertNotEquals(a1.getElemento(), "b1");
		Assert.assertNotEquals(a2.getElemento(), "b2");
		Assert.assertNotEquals(b.getElemento(), "a");
	}

	@Test
	public void setElementoDevePassar() {
		final NodoLista<String> alfabeto = new NodoLista<String>("Alfabeto");
		final NodoAbstrato<String> a = alfabeto.adicionar("a");
		final NodoAbstrato<String> a1 = a.adicionar("a1");
		final NodoAbstrato<String> a2 = a.adicionar("a2");
		final NodoAbstrato<String> b = alfabeto.adicionar("b");
		alfabeto.adicionar("c");

		a.setElemento("ac");
		a1.setElemento("a1c");
		a2.setElemento("a2c");
		b.setElemento("ab");
		
		Assert.assertEquals(a.getElemento(), "ac");
		Assert.assertEquals(a1.getElemento(), "a1c");
		Assert.assertEquals(a2.getElemento(), "a2c");
		Assert.assertEquals(b.getElemento(), "ab");
	}
	
}
