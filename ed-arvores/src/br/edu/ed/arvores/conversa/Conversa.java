package br.edu.ed.arvores.conversa;

import java.util.Scanner;

import br.edu.ed.arvores.binaria.NodoBinarioEncadeado;

public class Conversa {
	
	private NodoBinarioEncadeado<String> perguntas = new NodoBinarioEncadeado<>("Foi mal na prova de estruturas?");
	
	public static void main(String args[]) {
		Conversa perguntas = new Conversa();
		perguntas.executar();
	}
	
	public void executar() {

		NodoBinarioEncadeado<String> ponteiro = this.perguntas;
		
		Scanner scaner = new Scanner(System.in);
		
		do {
			
			System.out.println(ponteiro.getElemento() + " 1 - Sim | 2 - N�o");

			switch (scaner.nextInt()) {
			
				case 1:
					ponteiro = (NodoBinarioEncadeado<String>) ponteiro.getEsquerdo();
					break;
					
				case 2:
					ponteiro = (NodoBinarioEncadeado<String>) ponteiro.getDireito();
					break;
			}
			
		} while (ponteiro.interno());
		
		System.out.println(ponteiro.getElemento());
		
		scaner.close();
	}

	public Conversa() {
		
		this.perguntas
			.adicionar("Por falta de estudo?")
				.adicionar("Os exames te esperam!")
					.getPai()
						.adicionar("At� parece!")
							.getPai()
								.getPai()
									.adicionar("Sei, tem certeza?")
										.adicionar("Pare de mentir e v� estudar para o exame!")
											.getPai()
												.adicionar("N�o adianta ficar em d�vida, estude para os exames!");
	}

}
