package br.edu.ed.arvores;

import br.edu.aula.ed.IColecao;
import br.edu.aula.ed.Iterador;
import br.edu.udc.ed.vetor.Vetor;

public abstract class NodoAbstrato<E> {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((elemento == null) ? 0 : elemento.hashCode());
		result = prime * result + ((pai == null) ? 0 : pai.hashCode());
		result = prime * result + ((raiz == null) ? 0 : raiz.hashCode());
		return result;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodoAbstrato other = (NodoAbstrato) obj;
		if (elemento == null) {
			if (other.elemento != null)
				return false;
		} else if (!elemento.equals(other.elemento))
			return false;
		if (pai == null) {
			if (other.pai != null)
				return false;
		} else if (!pai.equals(other.pai))
			return false;
		if (raiz == null) {
			if (other.raiz != null)
				return false;
		} else if (!raiz.equals(other.raiz))
			return false;
		return true;
	}

	public NodoAbstrato(E elementoRaiz) {
		if(elementoRaiz == null) {
			throw new IllegalArgumentException("Uma �rvore precisa de um elemento raiz.");
		}
		
		this.setElemento(elementoRaiz);
		this.pai = null;
		this.raiz = this;
	}
	
	public NodoAbstrato(E elemento, NodoAbstrato<E> nodoPai) {
		if(nodoPai == null || elemento == null) {
			throw new IllegalArgumentException("Um nodo v�lido precisa de um pai e um elemento.");
		}
		
		this.pai = nodoPai;
		this.raiz = nodoPai.getRaiz();
		this.setElemento(elemento);
	}
	
	
	protected NodoAbstrato<E> raiz;
	protected NodoAbstrato<E> pai;
	protected E elemento;
	
	public int grau() {
		return this.getFilhos() != null ? this.getFilhos().tamanho() : 0;
	}
	
	public boolean externo() {
		return this.grau() == 0; 
	}
	
	public boolean interno() {
		return !this.externo();
	}
	
	public NodoAbstrato<E> getPai() {
		if(this.pai == null) {
			throw new IndexOutOfBoundsException("N�o cont�m um pai. � a raiz?");
		}
		return this.pai;
	}
	
	public NodoAbstrato<E> getRaiz() {
		if(this.raiz == null) {
			throw new IndexOutOfBoundsException("Nao existe nenhuma raiz definida.");
		}
		return (NodoAbstrato<E>) this.raiz;
	}
	
	public boolean irmaoDe(NodoAbstrato<E> nodo) {
		return this.getPai().getFilhos().contem(nodo);
	}
	
	public E getElemento() {
		return this.elemento;
	}
	
	public void setElemento(E elemento) {
		this.elemento = elemento;
	}
	
	public int profundidade() {
		return NodoAbstrato.profundidade(this);
	}
	
	public static <E> int profundidade(NodoAbstrato<E> nodo) {
		if(nodo.raiz()) {
			return 0;
		}
		
		return 1 + NodoAbstrato.profundidade(nodo.getPai());
	}
	
	public int altura() {
		return NodoAbstrato.altura(this);
	}
	
	public static <E> int altura(NodoAbstrato<E> nodo) {
		if(nodo.externo()) {
			return 0;
		}
		
		final Iterador<NodoAbstrato<E>> i = nodo.getFilhos().iterador();
		int maiorAlturaNodo = 0;
		while(i.temProximo()) {
			NodoAbstrato<E> nodoFilhoJ = i.proximo();
			if(i.proximo().interno()) {
				final NodoAbstrato<E> nodoFilho = i.proximo();
				final Iterador<NodoAbstrato<E>> j = nodoFilho.getFilhos().iterador();
				nodoFilhoJ = j.proximo();
			}
			
			final int alturaNodo = nodoFilhoJ.altura();
			
			if(alturaNodo > maiorAlturaNodo) {
				maiorAlturaNodo = alturaNodo;
			}
		}
		
		return 1 + maiorAlturaNodo;
	}
	
	public boolean raiz() {
		return this.getRaiz().equals(this);
	}
	
	public boolean formaArestaCom(NodoAbstrato<E> nodoDestino) {
		return NodoAbstrato.formaAresta(this, nodoDestino);
	}
	
	public static <E> boolean formaAresta(NodoAbstrato<E> nodoOrigem, NodoAbstrato<E> nodoDestino) {
		return nodoOrigem.getPai().equals(nodoDestino) || nodoDestino.getPai().equals(nodoOrigem);
	}
	
	public Vetor<NodoAbstrato<E>> caminho(NodoAbstrato<E> nodoDestino) {
		return NodoAbstrato.caminho(this, nodoDestino);
	}
	
	public static <E> Vetor<NodoAbstrato<E>> caminho(NodoAbstrato<E> nodoOrigem, NodoAbstrato<E> nodoDestino) {
		final Vetor<NodoAbstrato<E>> nodos = new Vetor<>();
		
		//Se for ascendente (pre-fixado)
		if(nodoOrigem.ancestralDe(nodoDestino)) {
			
			nodos.adiciona(nodoOrigem);
			
			final Iterador<NodoAbstrato<E>> i = nodoOrigem.getFilhos().iterador();
			while(i.temProximo()) {
				final NodoAbstrato<E> nodoFilho = i.proximo();
				
				if(nodoFilho.equals(nodoDestino)) {
					nodos.adiciona(nodoDestino);
					return nodos; //achou o destino
				} else {
					nodos.adiciona(nodoFilho.caminho(nodoDestino));
				}
			}
			
		//Se for descendente (pos-fixado)
		} else if(nodoOrigem.descendenteDe(nodoDestino)) {
			
			nodos.adiciona(nodoOrigem);
			final NodoAbstrato<E> nodoPai = nodoOrigem.getPai();
			
			if(nodoPai.equals(nodoDestino)) {
				nodos.adiciona(nodoDestino);
				return nodos; //achou o destino
			} else {
				nodos.adiciona(nodoPai.caminho(nodoDestino));
			}
		}
		
		return nodos;
	}
	
	public int comprimento(NodoAbstrato<E> nodoDestino) {
		return NodoAbstrato.caminho(this, nodoDestino).tamanho();
	}
	
	public boolean ancestralDe(NodoAbstrato<E> nodo) {
		return NodoAbstrato.temAscendencia(this, nodo);
	}
	
	public static <E> boolean temAscendencia(NodoAbstrato<E> nodoOrigem, NodoAbstrato<E> nodoDestino) {
		if(nodoDestino.getRaiz().equals(nodoOrigem)) return true;
		
		if(nodoOrigem.interno()) {
			final Iterador<NodoAbstrato<E>> i = nodoOrigem.getFilhos().iterador();
			while(i.temProximo()) {
				final NodoAbstrato<E> nodoFilho = i.proximo();
				if(nodoFilho.equals(nodoDestino) || NodoAbstrato.temAscendencia(nodoFilho, nodoDestino)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean descendenteDe(NodoAbstrato<E> nodo) {
		return NodoAbstrato.temDescendencia(this, nodo);
	}
	
	public static <E> boolean temDescendencia(NodoAbstrato<E> nodoOrigem, NodoAbstrato<E> nodoDestino) {
		if(nodoOrigem.raiz()) {
			return false;
		}
		
		final NodoAbstrato<E> nodoPai = nodoOrigem.getPai();
		if(nodoPai.equals(nodoDestino)) {
			return true;
		}
		
		return NodoAbstrato.temDescendencia(nodoPai, nodoDestino);
	}
	
	public abstract int tamanhoArvore();
	public abstract NodoAbstrato<E> adicionar(E elemento);
	public abstract IColecao<NodoAbstrato<E>> getFilhos();
	

}
