package br.edu.ed.arvores.binaria.testes;

import org.junit.Test;

import br.edu.ed.arvores.NodoAbstrato;
import br.edu.ed.arvores.binaria.NodoBinarioEncadeado;
import br.edu.udc.ed.vetor.Vetor;

import org.junit.Assert;

public class NodoBinarioEncadeadoTestes {
	
	@Test
	public void adicionaDevePassar() {
		final NodoBinarioEncadeado<String> organograma = new NodoBinarioEncadeado<String>("Fabio & Rosicler");
		
		organograma.adicionar("Julio")
		.getPai()
		.adicionar("Silvia")
		.adicionar("Damin")
		.getPai()
		.adicionar("Alessandra")
		.adicionar("Luciano")
		.getPai()
		.adicionar("Rodrigo");
		
		Assert.assertEquals(new NodoBinarioEncadeado<String>("Fabio & Rosicler"), organograma);
		Assert.assertEquals(7, organograma.tamanhoArvore());
		Assert.assertEquals(3, organograma.altura());
		
		final Vetor<NodoAbstrato<String>> filhos = organograma.getFilhos();
		Assert.assertEquals(2, filhos.tamanho());
		Assert.assertEquals(true, organograma.completo());
	}
	
	@Test
	public void getFilhosDevePassar() {
		final NodoBinarioEncadeado<String> dialogo = new NodoBinarioEncadeado<String>("Voce gosta de picol�?");
		
		dialogo.adicionar("Nao");
		
		final Vetor<NodoAbstrato<String>> filhos = dialogo.getFilhos();
		System.out.println(filhos);
		Assert.fail("Deveria dar erro.");
	}

}
