package br.edu.udc.ed.mapa.trabalho5;

import java.util.List;

import br.edu.udc.ed.mapa.Mapa;
import br.edu.udc.ed.mapa.Associacao;
import br.edu.udc.ed.vetor.Vetor;

public class MapaPessoas extends Mapa<Documento, Pessoa> {

	public Vetor<Pessoa> listarPessoas() {
		Vetor<Pessoa> pessoas = new Vetor<>();

		for (int i = 0; i < this.getTabela().size(); i++) {
			final List<Associacao<Documento, Pessoa>> vetor = this.getTabela().get(i);
			for (int j = 0; j < vetor.size(); j++) {
				pessoas.adiciona(vetor.get(j).getValor());
			}
		}

		return pessoas;
	}

	public Vetor<Documento> listarDocumentos() {
		Vetor<Documento> documentos = new Vetor<>();

		for (int i = 0; i < this.getTabela().size(); i++) {
			final List<Associacao<Documento, Pessoa>> vetor = this.getTabela().get(i);
			for (int j = 0; j < vetor.size(); j++) {
				documentos.adiciona(vetor.get(j).getChave());
			}
		}

		return documentos;
	}

	@Override
	public String toString() {
		StringBuffer acomulador = new StringBuffer();

		for (int i = 0; i < this.getTabela().size(); i++) {

			final List<Associacao<Documento, Pessoa>> vetor = this.getTabela().get(i);
			for (int j = 0; j < vetor.size(); j++) {

				final Associacao<Documento, Pessoa> acumulador = vetor.get(j);
				acomulador.append(acumulador.getValor().toString());
				acomulador.append(acumulador.getChave().toString());
				acomulador.append(String.format(" , "));
			}

			acomulador.append(String.format("\n"));
		}

		return acomulador.toString();
	}
	
	public void imprimir() {
		
		System.out.println("CAPACIDADE: " + this.getTabela().size());
		
		for (int i = 0; i < this.getTabela().size(); i++) {
			final List<Associacao<Documento, Pessoa>> associacoes = this.getTabela().get(i);
			
			if(associacoes.size() == 0) continue;
			
			System.out.println("Codigo: " + i + " --- Total: " + associacoes.size());
			
			for (int j = 0; j < associacoes.size(); j++) {
				System.out.println(associacoes.get(j));
			}
		}
	}

}
