package br.edu.udc.ed.mapa.trabalho5.testes;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.ed.mapa.trabalho5.Documento;
import br.edu.udc.ed.mapa.trabalho5.MapaPessoas;
import br.edu.udc.ed.mapa.trabalho5.Pessoa;

public class Trabalho5Testes {

	@Test
	public void mapaPessoaAdicionarDevePassar() {
		final MapaPessoas mapa = new MapaPessoas();

		final Pessoa pessoa = new Pessoa();
		pessoa.setNome("Spiderman");
		final Documento documento = new Documento();
		documento.setCpf(555578988);
		documento.setRg(454567854);

		mapa.adiciona(documento, pessoa);

		Assert.assertEquals(1, mapa.tamanho());
		Assert.assertTrue(mapa.contem(documento));
	}
	
	@Test
	public void mapaListarTodasPessoasDevePassar() {
		final MapaPessoas mapa = new MapaPessoas();

		final Pessoa pessoa = new Pessoa();
		pessoa.setNome("Carlos");
		final Documento documento = new Documento();
		documento.setCpf(555578988);
		documento.setRg(454567854);
		
		final Pessoa pessoa2 = new Pessoa();
		pessoa.setNome("Jose");
		final Documento documento2 = new Documento();
		documento.setCpf(45184156);
		documento.setRg(6894512);

		mapa.adiciona(documento, pessoa);
		mapa.adiciona(documento2, pessoa2);

		Assert.assertEquals(2, mapa.listarPessoas().tamanho());
		Assert.assertTrue(mapa.contem(documento));
	}
	
	@Test
	public void mapaListarTodosDocumentosDevePassar() {
		final MapaPessoas mapa = new MapaPessoas();

		final Pessoa pessoa = new Pessoa();
		pessoa.setNome("Carlos");
		final Documento documento = new Documento();
		documento.setCpf(555578988);
		documento.setRg(454567854);
		
		final Pessoa pessoa2 = new Pessoa();
		pessoa.setNome("Jose");
		final Documento documento2 = new Documento();
		documento.setCpf(45184156);
		documento.setRg(6894512);

		mapa.adiciona(documento, pessoa);
		mapa.adiciona(documento2, pessoa2);

		Assert.assertEquals(2, mapa.listarDocumentos().tamanho());
		Assert.assertTrue(mapa.contem(documento));
	}
	
	@Test
	public void mapaBalanceamento() {
		final MapaPessoas mapa = new MapaPessoas();
		
		for(int i = 0; i < 99999; i++) {
			final Pessoa pessoa = new Pessoa();
			pessoa.setNome(new BigInteger(20, new SecureRandom()).toString(32));
			final Documento documento = new Documento();
			Random random = new Random();
			documento.setCpf(random.nextInt((999999999 - 100000000) + 1) + 100000000);
			documento.setRg(random.nextInt((999999999 - 100000000) + 1) + 100000000);
			mapa.adiciona(documento, pessoa);
		}
		
		Assert.assertEquals(99999, mapa.tamanho());
		Assert.assertEquals(99999, mapa.listarDocumentos().tamanho());
		Assert.assertEquals(99999, mapa.listarPessoas().tamanho());
		
		mapa.imprimir();
	}

}
