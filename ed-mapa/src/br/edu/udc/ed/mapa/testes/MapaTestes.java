package br.edu.udc.ed.mapa.testes;

import org.junit.Test;
import org.junit.Assert;

import br.edu.udc.ed.mapa.Heroi;
import br.edu.udc.ed.mapa.Mapa;

public class MapaTestes {
	
	@Test
	public void testAdicionarDevePassar(){
		final Mapa<String, Heroi> herois = new Mapa<>();
		
		final Heroi spiderman = new Heroi();
		spiderman.setNome("Spiderman");
		herois.adiciona("XXMMOO", spiderman);
		
		final Heroi goku = new Heroi();
		goku.setNome("Goku");
		herois.adiciona("GODGOKU", goku);
		
		Assert.assertEquals(2, herois.tamanho());
		Assert.assertTrue( herois.contem("GODGOKU") );
		Assert.assertTrue( herois.contem("XXMMOO") );
 		Assert.assertEquals( herois.obtem("XXMMOO"), spiderman);
	}
	
	@Test
	public void testChaveIntegerDevePassar(){
		final Mapa<Integer, String> mapaInt = new Mapa<>();
		mapaInt.adiciona(1, "Um");
		mapaInt.adiciona(2, "Dois");
		mapaInt.adiciona(3, "Tr�s");
		
		Assert.assertEquals( mapaInt.obtem(1), "Um" );
		Assert.assertEquals( mapaInt.obtem(2), "Dois" );
		Assert.assertEquals( mapaInt.obtem(3), "Tr�s" );
	}

	@Test
	public void testAdicionaMilharesDevePassar(){
		final Mapa<String, Heroi> herois = new Mapa<>();

		for (int i = 0; i <99999; i++) {
			Heroi heroi = new Heroi();
			heroi.setNome("Homem-Aranha");
			herois.adiciona("QRCODE"+i, heroi);
		}

		Assert.assertEquals(99999, herois.tamanho() );
		System.out.println( herois );
	}



}
