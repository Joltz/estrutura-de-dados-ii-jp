package br.edu.udc.ed.arvore.arquivos.testes;

import org.junit.Test;
import org.junit.Assert;

import br.edu.udc.ed.arvore.arquivos.Arquivos;
import br.edu.udc.ed.arvore.arquivos.Data;

public class ArquivosTestes {
	Data dataCriacao = new Data(0, 0, 0);

	Arquivos root = new Arquivos(dataCriacao, "root", 100);

	Arquivos files = root.adicionar(dataCriacao, "files", 100);
	Arquivos execltaveis = files.adicionar(dataCriacao, "execultaveis", 50);
	Arquivos exe = execltaveis.adicionar(dataCriacao, ".exe", 20);

	Arquivos test = root.adicionar(dataCriacao, "test", 20);

	@Test
	public void caminho_casoFeliz() {
		Assert.assertEquals(root.toString(), "root");

		Assert.assertEquals(files.toString(), "root/files");
		Assert.assertEquals(execltaveis.toString(), "root/files/execultaveis");
		Assert.assertEquals(exe.toString(), "root/files/execultaveis/.exe");

		Assert.assertEquals(test.toString(), "root/test");
	}

	@Test
	public void tamanho_casoFeliz() {
		Assert.assertEquals(root.getTamanho(), 290);

		Assert.assertEquals(files.getTamanho(), 170);
		Assert.assertEquals(execltaveis.getTamanho(), 70);
		Assert.assertEquals(exe.getTamanho(), 20);

		Assert.assertEquals(test.getTamanho(), 20);
		
		test.setTamanho(10);
		
		Assert.assertEquals(test.getTamanho(), 10);
		Assert.assertEquals(root.getTamanho(), 280);
	}

}
