package br.edu.udc.ed.mapa.heroi;

import br.edu.udc.ed.vetor.Vetor;

public class MapaListaHeroi {

	private Vetor<AssociacaoHeroi> associacoes = new Vetor<>();

	public void adiciona(String qrcode, Heroi heroi) {
		if (!this.contem(qrcode)) {
			final AssociacaoHeroi associacao = new AssociacaoHeroi(qrcode, heroi);
			this.associacoes.adiciona(associacao);
		}
	}

	public void remove(String qrcode) {
		for (int i = 0; i < this.associacoes.tamanho(); i++) {
			final AssociacaoHeroi associacao = this.associacoes.obtem(i);
			if (qrcode.equals(associacao.getQrcode())) {
				this.associacoes.remove(i);
				return;
			}
		}
	}

	public Heroi obtem(String qrcode) {
		for (int i = 0; i < this.associacoes.tamanho(); i++) {
			final AssociacaoHeroi associacao = this.associacoes.obtem(i);
			if (associacao.getQrcode().equals(qrcode)) {
				return associacao.getHerio();
			}
		}
		throw new IllegalArgumentException("Nao EXISTE qr code");
	}

	public boolean contem(String qrcode) {
		for (int i = 0; i < this.associacoes.tamanho(); i++) {
			final AssociacaoHeroi associacao = this.associacoes.obtem(i);
			if (associacao.getQrcode().equals(qrcode))
				return true;
		}
		return false;
	}

	

	public int tamanho() {
		return this.associacoes.tamanho();
	}
}
