package br.edu.udc.ed.mapa;

public class DocumentoUnico {
	private int cfp;
	private int rg;
	
	public DocumentoUnico(int cpf , int rg) {
		this.rg = rg;
		this.cfp = cpf;
	}
	
	public int getCfp() {
		return cfp;
	}
	public void setCfp(int cfp) {
		this.cfp = cfp;
	}
	public int getRg() {
		return rg;
	}
	public void setRg(int rg) {
		this.rg = rg;
	}
	public String toString(){
		return String.format("CPF = %s / RG = %s\n", this.cfp,this.rg);
	}
	
}
